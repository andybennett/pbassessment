﻿using OrangeBricks.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrangeBricks.Web.Controllers.Property.Commands
{
    public class ScheduleViewingCommandHandler
    {
        private readonly IOrangeBricksContext _context;

        public ScheduleViewingCommandHandler(IOrangeBricksContext context)
        {
            _context = context;
        }

        public void Handle(ScheduleViewingCommand command)
        {
            var property = _context.Properties.Find(command.PropertyId);

            if (property.Viewings == null)
            {
                property.Viewings = new List<Viewing>();
            }

            var now = DateTime.Now;
            property.Viewings.Add(new Viewing
            {
                CreatedAt = now,
                UpdatedAt = now,
                Start = command.Start,
                End = command.Start.AddMinutes(30), // a default appointment time of 30 minutes
                BuyerUserId = command.BuyerUserId
            });

            _context.SaveChanges();
        }
    }
}