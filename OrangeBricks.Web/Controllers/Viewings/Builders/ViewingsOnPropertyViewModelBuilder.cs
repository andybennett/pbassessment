﻿using OrangeBricks.Web.Controllers.Viewings.ViewModels;
using OrangeBricks.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OrangeBricks.Web.Controllers.Viewings.Builders
{
    public class ViewingsOnPropertyViewModelBuilder
    {
        private readonly IOrangeBricksContext _context;

        public ViewingsOnPropertyViewModelBuilder(IOrangeBricksContext context)
        {
            _context = context;
        }

        public ViewingsOnPropertyViewModel Build(int id, string userId)
        {
            var property = _context.Properties
                .Where(p => p.Id == id)
                .Include(x => x.Viewings)
                .SingleOrDefault();

            var userIsSeller = string.Equals(property.SellerUserId, userId);
            var viewings = property.Viewings ?? new List<Viewing>();
            if (!userIsSeller)
            {
                viewings = viewings.Where(v => string.Equals(v.BuyerUserId, userId)).ToList();
            }

            return new ViewingsOnPropertyViewModel
            {
                Viewings = viewings.Select(v => new ViewingViewModel {
                    AppointmentTime = v.Start
                }),
                HasViewings = property.Viewings.Any(),
                StreetName = property.StreetName,
                PropertyType = property.PropertyType,
                Description = property.Description,
                NumberOfBedrooms = property.NumberOfBedrooms,
                IsUserTheSeller = userIsSeller,
                PropertyId = property.Id
            };
        }
    }
}