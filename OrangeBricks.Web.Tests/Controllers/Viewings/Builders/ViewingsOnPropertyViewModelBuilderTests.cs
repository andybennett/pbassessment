﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using NSubstitute;
using NUnit.Framework;
using OrangeBricks.Web.Controllers.Viewings.Builders;
using OrangeBricks.Web.Models;

namespace OrangeBricks.Web.Tests.Controllers.Viewings.Builders
{
    public static class ExtentionMethods
    {
        public static IDbSet<T> Initialize<T>(this IDbSet<T> dbSet, IQueryable<T> data) where T : class
        {
            dbSet.Provider.Returns(data.Provider);
            dbSet.Expression.Returns(data.Expression);
            dbSet.ElementType.Returns(data.ElementType);
            dbSet.GetEnumerator().Returns(data.GetEnumerator());
            return dbSet;
        }
    }

    [TestFixture]
    public class ViewingsOnPropertyViewModelBuilderTests
    {
        private IOrangeBricksContext _context;

        [SetUp]
        public void SetUp()
        {
            _context = Substitute.For<IOrangeBricksContext>();
        }

        [Test]
        public void BuildShouldReturnViewingsForAPropertyWhenTheSellerUserIdMatchingsWhatIsProvided()
        {
            // Arrange
            var userId = "Seller";
            var propertyId = 1;
            var builder = new ViewingsOnPropertyViewModelBuilder(_context);

            var properties = new List<Models.Property>{
                new Models.Property
                {
                    Id = propertyId,
                    SellerUserId = userId,
                    StreetName = "Smith Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer>(),
                    Viewings = new List<Viewing> {
                        new Viewing { BuyerUserId = "Buyer"},
                        new Viewing { BuyerUserId = "AnotherBuyer"}
                    }
                },
                new Models.Property
                {
                    Id = 2,
                    SellerUserId = "AnotherSeller",
                    StreetName = "Jones Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer>(),
                    Viewings = new List<Viewing>{
                        new Viewing { BuyerUserId = "Buyer"},
                        new Viewing { BuyerUserId = "AnotherBuyer"}
                    }
                }
            };

            var mockSet = Substitute.For<IDbSet<Models.Property>>()
                .Initialize(properties.AsQueryable());

            _context.Properties.Returns(mockSet);

            // Act
            var viewModel = builder.Build(propertyId, userId);

            // Assert
            Assert.That(viewModel.Viewings.Count, Is.EqualTo(2));
        }

        [Test]
        public void BuildShouldReturnOnlyViewingsForAPropertyWhenTheBuyerUserIdMatchesWhatIsProvided()
        {
            // Arrange
            var userId = "Buyer";
            var propertyId = 2;
            var builder = new ViewingsOnPropertyViewModelBuilder(_context);

            var properties = new List<Models.Property>{
                new Models.Property
                {
                    Id = 1,
                    SellerUserId = userId,
                    StreetName = "Smith Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer>(),
                    Viewings = new List<Viewing> {
                        new Viewing { BuyerUserId = "Buyer"},
                        new Viewing { BuyerUserId = "AnotherBuyer"}
                    }
                },
                new Models.Property
                {
                    Id = propertyId,
                    SellerUserId = "AnotherSeller",
                    StreetName = "Jones Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer>(),
                    Viewings = new List<Viewing>{
                        new Viewing { BuyerUserId = "Buyer"},
                        new Viewing { BuyerUserId = "AnotherBuyer"}
                    }
                }
            };

            var mockSet = Substitute.For<IDbSet<Models.Property>>()
                .Initialize(properties.AsQueryable());

            _context.Properties.Returns(mockSet);

            // Act
            var viewModel = builder.Build(propertyId, userId);

            // Assert
            Assert.That(viewModel.Viewings.Count, Is.EqualTo(1));
        }


        [Test]
        public void BuildShouldReturnNoViewingsForAPropertyWhenTheSellerUserIdDoesNotMatchWhatIsProvided()
        {
            // Arrange
            var userId = "Seller";
            var propertyId = 2;
            var builder = new ViewingsOnPropertyViewModelBuilder(_context);

            var properties = new List<Models.Property>{
                new Models.Property
                {
                    Id = 1,
                    SellerUserId = userId,
                    StreetName = "Smith Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer>(),
                    Viewings = new List<Viewing> {
                        new Viewing { BuyerUserId = "Buyer"},
                        new Viewing { BuyerUserId = "AnotherBuyer"}
                    }
                },
                new Models.Property
                {
                    Id = propertyId,
                    SellerUserId = "AnotherSeller",
                    StreetName = "Jones Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer>(),
                    Viewings = new List<Viewing>{
                        new Viewing { BuyerUserId = "Buyer"},
                        new Viewing { BuyerUserId = "AnotherBuyer"}
                    }
                }
            };

            var mockSet = Substitute.For<IDbSet<Models.Property>>()
                .Initialize(properties.AsQueryable());

            _context.Properties.Returns(mockSet);

            // Act
            var viewModel = builder.Build(propertyId, userId);

            // Assert
            Assert.That(viewModel.Viewings.Count, Is.EqualTo(0));
        }

        [Test]
        public void BuildShouldReturnNoViewingsForAPropertyWhenTheBuyerUserIdDoesNotMatchWhatIsProvided()
        {
            // Arrange
            var userId = "Buyer";
            var propertyId = 2;
            var builder = new ViewingsOnPropertyViewModelBuilder(_context);

            var properties = new List<Models.Property>{
                new Models.Property
                {
                    Id = propertyId,
                    SellerUserId = "Another Seller",
                    StreetName = "Smith Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer>(),
                    Viewings = new List<Viewing> {
                        new Viewing { BuyerUserId = "Another Buyer"},
                        new Viewing { BuyerUserId = "Yet Another Buyer"}
                    }
                },
                new Models.Property
                {
                    Id = 1,
                    SellerUserId = "AnotherSeller",
                    StreetName = "Jones Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer>(),
                    Viewings = new List<Viewing>{
                        new Viewing { BuyerUserId = "Buyer"},
                        new Viewing { BuyerUserId = "AnotherBuyer"}
                    }
                }
            };

            var mockSet = Substitute.For<IDbSet<Models.Property>>()
                .Initialize(properties.AsQueryable());

            _context.Properties.Returns(mockSet);

            // Act
            var viewModel = builder.Build(propertyId, userId);

            // Assert
            Assert.That(viewModel.Viewings.Count, Is.EqualTo(0));
        }
    }
}
