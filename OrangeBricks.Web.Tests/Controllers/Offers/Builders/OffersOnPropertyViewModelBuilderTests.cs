﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using NSubstitute;
using NUnit.Framework;
using OrangeBricks.Web.Controllers.Offers.Builders;
using OrangeBricks.Web.Models;

namespace OrangeBricks.Web.Tests.Controllers.Offers.Builders
{
    public static class ExtentionMethods
    {
        public static IDbSet<T> Initialize<T>(this IDbSet<T> dbSet, IQueryable<T> data) where T : class
        {
            dbSet.Provider.Returns(data.Provider);
            dbSet.Expression.Returns(data.Expression);
            dbSet.ElementType.Returns(data.ElementType);
            dbSet.GetEnumerator().Returns(data.GetEnumerator());
            return dbSet;
        }
    }

    [TestFixture]
    public class OffersOnPropertyViewModelBuilderTests
    {
        private IOrangeBricksContext _context;

        [SetUp]
        public void SetUp()
        {
            _context = Substitute.For<IOrangeBricksContext>();
        }

        [Test]
        public void BuildShouldReturnOffersForAPropertyWhenTheSellerUserIdMatchingsWhatIsProvided()
        {
            // Arrange
            var userId = "Seller";
            var propertyId = 1;
            var builder = new OffersOnPropertyViewModelBuilder(_context);

            var properties = new List<Models.Property>{
                new Models.Property
                {
                    Id = propertyId,
                    SellerUserId = userId,
                    StreetName = "Smith Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer> {
                        new Offer { BuyerUserId = "Buyer" },
                        new Offer { BuyerUserId = "Another Buyer" }
                    },
                    Viewings = new List<Viewing>()
                },
                new Models.Property
                {
                    Id = 2,
                    SellerUserId = "AnotherSeller",
                    StreetName = "Jones Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer> {
                        new Offer { BuyerUserId = "Buyer" },
                        new Offer { BuyerUserId = "Another Buyer" }
                    },
                    Viewings = new List<Viewing>()
                }
            };

            var mockSet = Substitute.For<IDbSet<Models.Property>>()
                .Initialize(properties.AsQueryable());

            _context.Properties.Returns(mockSet);

            // Act
            var viewModel = builder.Build(propertyId, userId);

            // Assert
            Assert.That(viewModel.Offers.Count, Is.EqualTo(2));
        }

        [Test]
        public void BuildShouldReturnOnlyOffersForAPropertyWhenTheBuyerUserIdMatchingsWhatIsProvided()
        {
            // Arrange
            var userId = "Buyer";
            var propertyId = 1;
            var builder = new OffersOnPropertyViewModelBuilder(_context);

            var properties = new List<Models.Property>{
                new Models.Property
                {
                    Id = propertyId,
                    SellerUserId = "Seller",
                    StreetName = "Smith Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer> {
                        new Offer { BuyerUserId = "Buyer" },
                        new Offer { BuyerUserId = "Another Buyer" }
                    },
                    Viewings = new List<Viewing>()
                },
                new Models.Property
                {
                    Id = 2,
                    SellerUserId = "AnotherSeller",
                    StreetName = "Jones Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer> {
                        new Offer { BuyerUserId = "Buyer" },
                        new Offer { BuyerUserId = "Another Buyer" }
                    },
                    Viewings = new List<Viewing>()
                }
            };

            var mockSet = Substitute.For<IDbSet<Models.Property>>()
                .Initialize(properties.AsQueryable());

            _context.Properties.Returns(mockSet);

            // Act
            var viewModel = builder.Build(propertyId, userId);

            // Assert
            Assert.That(viewModel.Offers.Count, Is.EqualTo(1));
        }

        [Test]
        public void BuildShouldReturnNoOffersForAPropertyWhenTheSellerUserIdMatchingsWhatIsProvided()
        {
            // Arrange
            var userId = "Seller";
            var propertyId = 1;
            var builder = new OffersOnPropertyViewModelBuilder(_context);

            var properties = new List<Models.Property>{
                new Models.Property
                {
                    Id = propertyId,
                    SellerUserId = "Another Seller",
                    StreetName = "Smith Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer> {
                        new Offer { BuyerUserId = "Buyer" },
                        new Offer { BuyerUserId = "Another Buyer" }
                    },
                    Viewings = new List<Viewing>()
                },
                new Models.Property
                {
                    Id = 2,
                    SellerUserId = userId,
                    StreetName = "Jones Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer> {
                        new Offer { BuyerUserId = "Buyer" },
                        new Offer { BuyerUserId = "Another Buyer" }
                    },
                    Viewings = new List<Viewing>()
                }
            };

            var mockSet = Substitute.For<IDbSet<Models.Property>>()
                .Initialize(properties.AsQueryable());

            _context.Properties.Returns(mockSet);

            // Act
            var viewModel = builder.Build(propertyId, userId);

            // Assert
            Assert.That(viewModel.Offers.Count, Is.EqualTo(0));
        }

        [Test]
        public void BuildShouldReturnNoOffersForAPropertyWhenTheBuyerUserIdMatchingsWhatIsProvided()
        {
            // Arrange
            var userId = "Buyer";
            var propertyId = 1;
            var builder = new OffersOnPropertyViewModelBuilder(_context);

            var properties = new List<Models.Property>{
                new Models.Property
                {
                    Id = propertyId,
                    SellerUserId = "Another Seller",
                    StreetName = "Smith Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer> {
                        new Offer { BuyerUserId = "Another Buyer" },
                        new Offer { BuyerUserId = "Yet Another Buyer" }
                    },
                    Viewings = new List<Viewing>()
                },
                new Models.Property
                {
                    Id = 2,
                    SellerUserId = "Yet Another Seller",
                    StreetName = "Jones Street",
                    Description = "",
                    IsListedForSale = true,
                    Offers = new List<Offer> {
                        new Offer { BuyerUserId = "Buyer" },
                        new Offer { BuyerUserId = "Another Buyer" }
                    },
                    Viewings = new List<Viewing>()
                }
            };

            var mockSet = Substitute.For<IDbSet<Models.Property>>()
                .Initialize(properties.AsQueryable());

            _context.Properties.Returns(mockSet);

            // Act
            var viewModel = builder.Build(propertyId, userId);

            // Assert
            Assert.That(viewModel.Offers.Count, Is.EqualTo(0));
        }
    }
}
