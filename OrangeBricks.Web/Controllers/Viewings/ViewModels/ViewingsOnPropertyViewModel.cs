﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrangeBricks.Web.Controllers.Viewings.ViewModels
{
    public class ViewingsOnPropertyViewModel
    {
        public int PropertyId { get; set; }
        public string StreetName { get; set; }
        public string Description { get; set; }
        public int NumberOfBedrooms { get; set; }
        public string PropertyType { get; set; }
        public bool HasViewings { get; set; }
        public bool IsUserTheSeller { get; set; }
        public IEnumerable<ViewingViewModel> Viewings { get; set; }
    }
}