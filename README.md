# Purplebricks Developer Test

Submission in response to the Purple Bricks Developer Test for Andy Bennett.

The content of the original Readme.md file containing the instructions, can now be found in instructions.txt.

The response to Objective 3, the code review, can be found in Objective3CodeReview.txt.