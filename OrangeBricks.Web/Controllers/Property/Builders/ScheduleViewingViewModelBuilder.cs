﻿using OrangeBricks.Web.Controllers.Property.ViewModels;
using OrangeBricks.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrangeBricks.Web.Controllers.Property.Builders
{
    public class ScheduleViewingViewModelBuilder
    {
        private readonly IOrangeBricksContext _context;

        public ScheduleViewingViewModelBuilder(IOrangeBricksContext context)
        {
            _context = context;
        }

        public ScheduleViewingViewModel Build(int id)
        {
            var property = this._context.Properties.Find(id);

            return new ScheduleViewingViewModel {
                PropertyId = property.Id,
                StreetName = property.StreetName,
                PropertyType = property.PropertyType,
                Start = DateTime.Now
            };
        }
    }
}