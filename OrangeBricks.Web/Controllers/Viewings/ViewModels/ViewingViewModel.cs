﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrangeBricks.Web.Controllers.Viewings.ViewModels
{
    public class ViewingViewModel
    {
        public DateTime AppointmentTime { get; set; }
    }
}